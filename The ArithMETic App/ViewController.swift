//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by Varun Anugu  on 2/14/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    var ActivityValue:String="Select Activity"
    let Activity:[String] = ["Select Activity","Bicycling","Jumping rope","Running - slow","Running - fast","Tennis","Swimming"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Activity.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ActivityValue = Activity[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Activity[row]
    }
    
    
    @IBOutlet weak var ActivityPV: UIPickerView!
    
    
    @IBOutlet weak var WeightTF: UITextField!
    
    
    @IBOutlet weak var ExerciseTimeTF: UITextField!
    
    
    @IBOutlet weak var EnergyLBL: UILabel!
    
    
    @IBOutlet weak var TimeLBL: UILabel!
    
    
    @IBAction func CalculateBTN(_ sender: Any) {
        let weight : Double? = Double(WeightTF.text!)
        let exerciseTime : Int? = Int(ExerciseTimeTF.text!)
        
        if weight != nil && exerciseTime != nil && ActivityValue != "Select Activity"
        {
            let exerciseTime = ExerciseCoach.energyConsumed(during: ActivityValue, weight: weight!, time: exerciseTime!)
            let Time = ExerciseCoach.timeToLose1Pound(during: ActivityValue, weight: weight!)
            
            EnergyLBL.text = String(format:"%.1f",exerciseTime)+" "+"cal"
            TimeLBL.text = String(format:"%.1f",Time)+" "+"minutes"
        }
        else if(weight == nil)
        {
            WeightTF.text = "Please provide correct weight"
        }
        else{
            ExerciseTimeTF.text = "Please provide correct time"
        }
        
    }
    
    @IBAction func ClearBTN(_ sender: Any) {
        
        WeightTF.text = ""
        ExerciseTimeTF.text = ""
        ActivityValue = "Select Activity"
        EnergyLBL.text = "0 cal"
        TimeLBL.text = "0 minutes"
        ActivityPV.selectRow(0, inComponent: 0, animated: true)
        
    }
}
    
    struct ExerciseCoach{
        static func energyConsumed(during: String, weight: Double, time: Int) -> Double{
            let sports : [String : Double] = ["Bicycling" :8.0, "Jumping Rope": 12.3, "Running - slow": 9.8, "Running - fast": 23.0, "Tennis": 8.0, "Swimming": 5.8 ]
            let met = sports[during]
            let energyConsumed = met! * 3.5 * 150/2.2/200 * Double(time)
            return energyConsumed
        }
        
        static func timeToLose1Pound(during: String, weight: Double) -> Double
        {
            let sports : [String : Double] = ["Bicycling" :8.0, "Jumping Rope": 12.3, "Running - slow": 9.8, "Running - fast": 23.0, "Tennis": 8.0, "Swimming": 5.8 ]
            let met = sports[during]
            let timeToBurn = 3500 / (met! * 3.5 * 150/2.2/200)
            return timeToBurn
        }
        
    }
    
    


